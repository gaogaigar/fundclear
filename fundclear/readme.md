- install python
- cd /d ${PYTHON_HOME}
    ```bash
    py -m venv fundclear
    fundclear\Scripts\activate.bat
    # install package
    pip install -U selenium
    pip install pandas
    pip install openpyxl
    pip install beautifulsoup4
    pip install pyinstaller
    ```

    ```bash
    pyinstaller -F Fundclear.py
    pyinstaller --onefile --windowed Fundclear.py
    ```
- 本機使用chrome version 需與 selenium chrome driver 相同 
  - [chromedriver](https://chromedriver.chromium.org) 