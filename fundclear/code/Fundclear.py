# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from pandas.core.frame import DataFrame
from pandas import DataFrame as df
from bs4 import BeautifulSoup

import os
import configparser
import time

# read driver path
config = configparser.ConfigParser()
config.read(os.getcwd() + '\\Fundclear.properties')

driver = config.get(section='setting', option='driver_path')
save = config.get(section='setting', option='save_path')

print('driver_path : ' + driver)
print('save_path : ' + save)

url = 'https://announce.fundclear.com.tw/MOPSFundWeb/I01.jsp'

options = webdriver.ChromeOptions()
options.add_argument("start-maximized")

# 20211225 fix for 'Google Chrome cannot read and write to its data directory : selenium'
# https://stackoverflow.com/questions/67198271/google-chrome-cannot-read-and-write-to-its-data-directory-selenium
dir_path = os.getcwd()
options.add_argument(f'user-data-dir={dir_path}/selenium') 
# options.add_argument("--user-data-dir=chrome-data")
options.add_argument("--disable-extensions")

browser = webdriver.Chrome(chrome_options=options, executable_path=driver)
browser.get(url)

time.sleep(10)  # avoid reCAPTCHA error

browser.find_element_by_css_selector("input[type='submit']").click()

timeout = 60
resultLocator = '/html/body/form[2]/table/tbody/tr[2]/td/table/tbody/tr'

try:
    # 等待網頁搜尋結果
    WebDriverWait(browser, timeout).until(EC.visibility_of_element_located((By.XPATH, resultLocator)))

    columns = []    # 表頭

    # 搜尋結果  -   表頭
    td = browser.find_element_by_xpath("/html/body/form[2]/table/tbody/tr[2]/td/table/tbody/tr/td/h1[1]/table/tbody/tr[1]/td[1]")
    columns.append(td.text)

    headers = browser.find_element_by_xpath("/html/body/form[2]/table/tbody/tr[2]/td/table/tbody/tr/td/h1[1]/table/tbody/tr[2]")
    for td in headers.find_elements_by_tag_name('td'):
        columns.append(td.text)

    # 搜尋結果  -   資料
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    result = []     # 資料
    for tr in soup.select("tr[class^='row']"):
        values = []

        for td in tr.select('td'):
            values.append(td.text)

        print(values)    
        result.append(values)            

        df = DataFrame(columns=columns, data=result)

    df.to_excel(save, index=False)

except TimeoutException:
    print('等待逾時！')
finally:
    browser.close()
